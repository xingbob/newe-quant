# README #

This repo is for NeweWM QUANT code.

### Start developing ###

* Clone the repo to your local folder

```
#!bash
$ git clone https://xingbob@bitbucket.org/xingbob/newe-quant.git
```

* Change directory into the repo

```
#!bash
$ cd newe-quant
```

* Checkout a development branch

```
#!bash
$ git checkout my-dev
```

* Add a file that has been changed to a commit

```
#!bash
$ git add my-file.py
```

* Commit the changes locally

```
#!bash
$ git commit -m "commit message"
```

* Push the commit to remote master branch

```
#!bash
$ git push origin master
```